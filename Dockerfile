FROM debian:stable-slim

RUN addgroup --system --gid 66547 heritas;\
    adduser --system --uid 1337 --gid 66547 user
RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-transport-https \
    ca-certificates \
    software-properties-common \
    curl \
    nfs-common \
    wget \
    procps \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN curl -L https://basemount.basespace.illumina.com/install/ > basemount.sh
RUN bash basemount.sh

RUN apt-get install -y unzip
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"  && \
    unzip awscliv2.zip && \
    ./aws/install

ENV PATH /usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/home/user/.local/bin
ENV HOME=/home/user

WORKDIR /home/user
COPY generar_csv.sh .
COPY copiar_fastq_basespace.sh .
RUN mkdir -p /tmp/basespace_account
RUN mkdir -p /root/.basespace

