#!/bin/bash

# Establecer salida al encontrar error
set -e

BSPACE_MOUNTPOINT="/tmp/basespace_account"
BUCKET_NAME="heritas-nextflow"

montar_basespace(){
    echo "Obteniendo parametro"
    parameter_name="/heritas/basespace/bioinformatica_heritas.com.ar/accessToken"

    parameter_value=$(aws ssm get-parameter --name "$parameter_name" --with-decryption --query 'Parameter.Value' --output text)

    if [ -n "$parameter_value" ]; then
      echo "$parameter_value" > /root/.basespace/default.cfg
      # echo "$parameter_value" > /home/user/.basespace/default.cfg
      echo "El valor del parámetro se ha guardado en default.cfg"
    else
      echo "No se pudo obtener el valor del parámetro"
      exit 1
    fi

    echo "Montando Basespace"
    basemount $BSPACE_MOUNTPOINT
    # cd $BSPACE_MOUNTPOINT
}

leer_samples_csv(){
    # CSV en bucket 
    SAMPLESHEET_BUCKET="s3://heritas-nextflow/mapping_inputs/samplesheet_input.csv"
    SAMPLESHEET="AWS_fastq_samplesheet.csv"

    # Descargar Samplesheet
    aws s3 cp $SAMPLESHEET_BUCKET "./$SAMPLESHEET"

    # Leer el archivo CSV
    LEER_ENCABEZADOS=true
    while IFS=',' read -r -a linea; do
        if $LEER_ENCABEZADOS; then
            LEER_ENCABEZADOS=false
        else
        run=${linea[0]}
        sample=${linea[1]}
        descargar_fastq_basespace $run $sample
        fi

    done < "$SAMPLESHEET"
}

descargar_fastq_basespace(){
  local run="$1"
  local sample="$2"
  echo "Buscando RUN:${run} Sample:${sample}"

  local PATH_SAMPLE="$BSPACE_MOUNTPOINT/Projects/$run/Samples/$sample"
  if [ -d "$PATH_SAMPLE" ]; then
        echo "Se encontró el Sample $sample"
        for FASTQ in $(ls "$PATH_SAMPLE/Files/"); do
            local FASTQ_RENAMED=$(renombrar_archivo_s3 $FASTQ)
            echo "Copiando fastq -> $busqueda/Files/$FASTQ to s3://heritas-nextflow/fastq_files/$FASTQ"
            echo "Cambiando nombre -> $FASTQ to $FASTQ_RENAMED"
            copy_file_to_s3 "$PATH_SAMPLE/Files/$FASTQ" "s3://$BUCKET_NAME/fastq_files/$sample/$FASTQ_RENAMED"
        done
        wait
  else
    echo "No se encontró el sample $sample en $PATH_SAMPLE"
  fi
  echo "Copiar fastq files terminado correctamente"
}

copy_file_to_s3() {
    local file_path="$1"
    local bucket_path="$2"
    if ! [[ $(aws s3 cp $file_path $bucket_path --region us-east-1 &) ]];then
        exit 1
    fi
}

renombrar_archivo_s3(){
  nombre_original=$1
  sample="${nombre_original%%_*}"
  l_numero=$(echo "$nombre_original" | awk -F '_' '{print $3}')
  r_numero=$(echo "$nombre_original" | awk -F '_' '{print $4}')
  
  # Crear el nuevo nombre de archivo
  nuevo_nombre="${sample}_${l_numero}_${r_numero}.fastq.gz"
  
  # Renombrar el archivo
  echo $nuevo_nombre
}

generar_csv(){
    CSV_ENTRADA="s3://heritas-nextflow/mapping_inputs/samplesheet_input.csv"

    echo -e "Descargando CSV:\n\t $CSV_ENTRADA"
    # Descargar archivo csv
    if ! [[ $(aws s3 cp $CSV_ENTRADA ./input.csv --region us-east-1) ]];then
        exit 1
    fi

    # Columnas del nuevo csv
    echo "patient,sample,lane,fastq_1,fastq_2" > AWS_samplesheet.csv

    # Leer el archivo CSV
    LEER_ENCABEZADOS=true
    while IFS=',' read -r -a linea; do
        if $LEER_ENCABEZADOS; then
            LEER_ENCABEZADOS=false
        else
        run=${linea[0]}
        sample=${linea[1]}
        # Verificar si las variables no están vacías ni contienen solo espacios
        # if [ -n "$variable1" ] && [ -n "$variable2" ]; then
        if [[ -n "${run}" && ! "${run}" =~ ^\ *$ ]] && [[ -n "${sample}" && ! "${sample}" =~ ^\ *$ ]]; then
            for ((i=1; i<=4;i++))
            do
                # Generar filas del csv
                echo -e "$sample,$sample,lane_${i},s3://heritas-nextflow/fastq_files/$sample/${sample}_L00${i}_R1.fastq.gz,s3://heritas-nextflow/fastq_files/$sample/${sample}_L00${i}_R2.fastq.gz" >> AWS_samplesheet.csv
            done
        fi
        fi
    done < "input.csv"

    # Subir CSV generado
    BUCKET_PATH="s3://heritas-nextflow/mapping_inputs/AWS_samplesheet.csv"
    if ! [[ $(aws s3 cp AWS_samplesheet.csv $BUCKET_PATH --region us-east-1 ) ]];then
        exit 1
    fi
    echo -e "CSV generado correctamente:\n\t$BUCKET_PATH"
}

main(){
    generar_csv
    montar_basespace
    leer_samples_csv
    exit 0
}

main

