#!/bin/bash

# Establecer salida al encontrar error
set -e

# Nombre del archivo CSV
CSV_ENTRADA="s3://heritas-nextflow/mapping_inputs/samplesheet_input.csv"

echo -e "Descargando CSV:\n\t $CSV_ENTRADA"
# Descargar archivo csv
if ! [[ $(aws s3 cp $CSV_ENTRADA ./input.csv --region us-east-1) ]];then
    exit 1
fi

# Columnas del nuevo csv
echo "patient,sample,lane,fastq_q,fastq_2,run" > AWS_samplesheet.csv

# Leer el archivo CSV
LEER_ENCABEZADOS=true
while IFS=',' read -r -a linea; do
    if $LEER_ENCABEZADOS; then
        LEER_ENCABEZADOS=false
    else
    run=${linea[0]}
    sample=${linea[1]}
    for ((i=1; i<=4;i++))
    do
        # Generar filas del csv
        echo -e "$sample,$sample,lane_${i},s3://heritas-nextflow/fastq_files/$sample/${sample}_L00${i}_R1.fastq.gz,s3://heritas-nextflow/fastq_files/$sample/${sample}_L00${i}_R2.fastq.gz,${run}" >> AWS_samplesheet.csv
    done
    fi
done < "input.csv"

# Subir CSV generado
BUCKET_PATH="s3://heritas-nextflow/mapping_inputs/AWS_samplesheet.csv"
if ! [[ $(aws s3 cp AWS_samplesheet.csv $BUCKET_PATH --region us-east-1 ) ]];then
    exit 1
fi
echo -e "CSV generado correctamente:\n\t$BUCKET_PATH"

