nextflow.enable.dsl=2

process GET_FASTQ_FILES{
    container "public.ecr.aws/h1n0h1x4/download_fastq:latest"
    containerOptions '--privileged'
    memory '4 GB'
    cpus 2 

    script:
    """
    /home/user/copiar_fastq_basespace.sh
    """
}

workflow {
    GET_FASTQ_FILES()
}
